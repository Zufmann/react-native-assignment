import React from 'react';
import MeasurePage from './app/components/measure-page'
import Provider from "react-redux/es/components/Provider";
import store from "./app/store";

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MeasurePage/>
      </Provider>
    );
  }
}