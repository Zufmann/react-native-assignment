import {
  FETCH_MEASURES,
  FETCH_MEASURES_SUCCESS,
} from '../constants/action-types'

export let loadMeasures = searchConditions => ({ type: FETCH_MEASURES, searchConditions });

export let loadMeasuresSuccess = measures => ({ type: FETCH_MEASURES_SUCCESS, measures });