import {
  FETCH_MEASURES_SUCCESS,
  LOGOUT,
} from '../constants/action-types'

export default function measures(state = {measures: {}}, action = {}) {
  let measures;
  switch (action.type) {
    case FETCH_MEASURES_SUCCESS:
      measures = action.measures || {};
      Object.keys(measures).forEach(feature => {
        const features = measures[feature];
        measures[feature] = {
        pieces: features,
        columns: features.length > 3 ? 2:1,
        size:features.length > 1? 2:1
      }});
      return {measures};
    case LOGOUT:
      return {measures: {}};
    default:
      return state
  }
}
