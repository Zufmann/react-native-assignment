import connect from "react-redux/es/connect/connect";
import React from "react";
import {StyleSheet, Text, View} from "react-native";
import MeasurePiece from "./measure-piece";

class MeasureFeature extends React.Component {
  render() {
    const feature = this.props.measures[this.props.feature];

    const specificStyles = StyleSheet.create({
      feature:{
        padding:3,
        flexGrow: feature.columns
      },
      title:{
        backgroundColor: feature.fail?'red':'green',
        color:'white',
        textTransform: 'uppercase'
      }
    });

    let columns = [];
    if(feature.columns === 2) {
      const len= feature.pieces.length;
      columns.push(feature.pieces.slice(0, len / 2));
      columns.push(feature.pieces.slice(len / 2, len))
    } else {
      columns.push(feature.pieces)
    }


    return (
      <View style={specificStyles.feature}>
        <Text style={specificStyles.title}> {this.props.feature} </Text>
        <View style={styles.columns}>
          {columns.map(this.printColumn)}
        </View>
      </View>
    );
  }

  printColumn(column, key){
    return(
      <View key={key} style={styles.column}>
        <View style={styles.header}>
          <Text style={styles.text}>Control</Text>
          <Text style={styles.text}>Dev</Text>
          <Text style={styles.text}>Dev Out Total</Text>
          <Text style={styles.text}/>
        </View>
        {column.map((piece, i) => {
          return (<MeasurePiece key={i} piece={piece}/>)
        })}
      </View>
    )
  }

}

const styles = StyleSheet.create({
    columns:{
      flexDirection: 'row',
      justifyContent: 'space-around',
      backgroundColor: 'silver'
    },
    column:{
      flexGrow: 1
    },
    header:{
      flexDirection: 'row',
      alignItems: 'flex-start',
      justifyContent: 'space-around',
    },
    text:{
      flexBasis: 100,
    },
  });

const mapStateToProps = state => {
  const measures = state.measures.measures || {};
  return { measures };
};

export default connect(mapStateToProps)(MeasureFeature)