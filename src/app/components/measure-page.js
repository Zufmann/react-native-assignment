import {bindActionCreators} from "redux";
import {loadMeasures} from "../actions/measures";
import connect from "react-redux/es/connect/connect";
import React from "react";
import {StyleSheet, Text, View} from "react-native";
import MeasureFeature from "./measure-feature";

class MeasurePage extends React.Component {
  componentDidMount() {
    this.props.loadMeasures();
  }

  render() {
    let buffer;
    let features = [];
    Object.keys(this.props.measures).forEach((key, i) => {
      const size = this.props.measures[key].size;
      if(size > 1) {
        features.push(<MeasureFeature key={i} feature={key}/>)
      } else if(buffer){
        features.push(
          <View key={i} style={styles.join}>
            {buffer}
            <MeasureFeature key={i} feature={key}/>
          </View>
        );
        buffer = null
      } else {
        buffer = (<MeasureFeature key={i} feature={key}/>)
      }
    });
    if(buffer){
      features.push(buffer)
    }

    return (
      <View>
        <Text> Part A </Text>
        <View style={styles.features}>
          {features}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  features:{
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
  },
  join:{
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'space-around',
    flexGrow:1,
  }
});

const mapStateToProps = state => {
  const measures = state.measures.measures || {};
  return { measures };
};
const mapDispatchToProps = dispatch => bindActionCreators({ loadMeasures  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MeasurePage)