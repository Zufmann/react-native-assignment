import {StyleSheet} from "react-native";
import connect from "react-redux/es/connect/connect";
import React from "react";
import {Text, View} from "react-native";

class MeasurePiece extends React.Component {
  render() {
    return (
      <View>
        {Object.keys(this.props.piece).map((key, i) => {
          const piece = this.props.piece[key];
          return (
            <View key={i} style={styles.piece}>
              <Text style={styles.text}>{key}</Text>
              <Text style={styles.text}>{piece.dev}</Text>
              <Text style={styles.text}>{piece.devOutTotal}</Text>
              <Text style={styles.text}>{piece.control?'pass':'fail'}</Text>
            </View>
          )
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  piece:{
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
  },
  text:{
    flexBasis: 100,
    padding: 2,
    paddingLeft: 4
  }
});

const mapStateToProps = state => {
  const measures = state.measures.measures || {};
  return { measures };
};

export default connect(mapStateToProps)(MeasurePiece)