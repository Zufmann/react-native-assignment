import { takeEvery, put } from "redux-saga/effects";
import {FETCH_MEASURES} from '../constants/action-types'
import {loadMeasuresSuccess} from "../actions/measures";

const sizes = [6, 1, 1, 1, 1, 3];
const MAX_DEVIATION = 3;

export function* getMeasuresSaga() {
  yield takeEvery(FETCH_MEASURES, fetchMeasuresWorker);
}

function*  fetchMeasuresWorker(searchConditions) {
  while(true) {
    const payload = yield callPrepareMeasures(searchConditions);
    yield put(loadMeasuresSuccess(payload));
  }
}

function generateRandom(){
  const unif = Math.random();
  const beta = Math.pow(Math.sin(unif*Math.PI/2),3);
  return (beta < 0.5) ? 2*beta : 2*(1-beta);

}

function generateMeasure() {
  const measure =  {
    dev: (generateRandom() * 10).toFixed(2),
    devOutTotal:generateRandom().toFixed(2),
  };
  measure.control = measure.dev < MAX_DEVIATION;
  return measure;
}

function generatePieceMeasure() {
  return {
    x: generateMeasure(),
    y: generateMeasure(),
    z: generateMeasure(),
    diameter: generateMeasure(),
  };
}

let delay;
async function callPrepareMeasures(searchConditions) {
  if(delay) {
    await delay(5000);
  } else {
    delay = ms => new Promise(res => setTimeout(res, ms));
  }
  //ToDo: replace by the actual call to server
  return sizes.reduce((acc, size, i) => {
    const name = `Feature ${i}`;
    acc[name] = new Array(size).fill(0).map(() => generatePieceMeasure());
    return acc;
  }, {});
}