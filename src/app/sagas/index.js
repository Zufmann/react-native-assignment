import { all, fork } from "redux-saga/effects";
import { getMeasuresSaga } from './measures';

export default function* rootSaga() {
  yield all([
    getMeasuresSaga,
  ].map(fork));
}
