This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Next Steps:

    · Feature header color depending on expected quality

    · Replace the current 'pass/fail' with icons

    · Check the margins, paddings and such to be more pixel perfect

    · Dockerize it


To execute on localhost, just execute 2 instructions:
##### `npm install`
##### `npm start`

To check the behaviour with different amounts of features & pieces, on the file app/sagas/measures is
an array "sizes" with the info of how many features and pieces there are, you can change it

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
